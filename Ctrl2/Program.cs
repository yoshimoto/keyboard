﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace Ctrl2
{
    class Program
    {
        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(int nVirtKey);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {
            public Int32 X;
            public Int32 Y;
        };
        public static Point GetMousePosition()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }
        static private bool GetCtrlKey()
        {
            //Ctrl
            var ctrl_state = GetAsyncKeyState(0x11);
            if ((ctrl_state & 0x8000) != 0) return true;
            else return false;
        }
        static private bool CheckPreposition(string str)
        {
            var prepositions = new string[]{
                "about","aboard","above","across","after","against","along","alongside","amid","among","anti","around","as","at",
                "bar","before","behind","below","beneath","beside","besides","between","beyond","but","by",
                "considering",
                "despite","down","during",
                "except",
                "for","from",
                "in","inside","into",
                "less","like",
                "minus",
                "near","notwithstanding",
                "of","off","on","onto","opposite","out","outside","over",
                "pace","past","pending","per","plus",
                "re","regarding","round",
                "save","saving","since",
                "than","through","throughout","till","to","touching","toward",
                "under","underneath","unless","unlike","until","up",
                "versus","via","vice",
                "with","within","without"};
            if (Array.IndexOf(prepositions, str) >= 0)
            {
                return true;
            }
            return false;
        }
        static private bool GetOtherKey()
        {
            for (int i = 0; i < 10; i++)
            {
                var item = 0x30 + i;
                var state = GetAsyncKeyState(item);
                if ((state & 0x8000) != 0)
                {
                    Console.Write(i);
                    return true;
                }
            }
            for (int i = 0; i < 26; i++)
            {
                var item = 0x41 + i;
                var state = GetAsyncKeyState(item);
                if ((state & 0x8000) != 0)
                {
                    Console.Write(item);
                    return true;
                }
            }
            for (int i = 0; i < 10; i++)
            {
                var item = 0x60 + i;
                var state = GetAsyncKeyState(item);
                if ((state & 0x8000) != 0)
                {
                    Console.Write(item);
                    return true;
                }
            }
            //Backspace,Tab,Enter,Shift,Alt,Esc,PageUp,PageDown,End,Home,Left,Up,Right,Down(0x28)
            //PrintScreen,Insert,Delete,Win,Win,App,*,+,-,.,/
            var list = new int[] { 0x08, 0x09, 0x0D, 0x10, 0x12, 0x1B, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                0x2C, 0x2D, 0x2E, 0x5B, 0x5C, 0x5D, 0x6A, 0x6B, 0x6D, 0x6E, 0x6F };
            foreach (var item in list)
            {
                var state = GetAsyncKeyState(item);
                if ((state & 0x8000) != 0)
                {
                    Console.Write(item);
                    return true;
                }
            }
            return false;
        }
        static Window window;
        [STAThread]
        static void Main(string[] args)
        {
            string mutexName = "Ctrl2";
            var mutex = new System.Threading.Mutex(false, mutexName);
            bool hasHandle = false;
            try
            {
                try { hasHandle = mutex.WaitOne(0, false); }
                catch (System.Threading.AbandonedMutexException) { hasHandle = true; }
                if (hasHandle == false) { return; }

                long i = 0;
                while (true)
                {
                    System.Threading.Thread.Sleep(50);
                    bool flag = false;
                    bool ctrl_state = GetCtrlKey();
                    bool other_state = GetOtherKey();
                    if (ctrl_state && !other_state)
                    {
                        if (i > 1)
                        {
                            while (true)
                            {
                                ctrl_state = GetCtrlKey();
                                other_state = GetOtherKey();
                                if (!ctrl_state && !other_state)
                                {
                                    flag = true; i = 0;
                                    break;
                                }
                                else if (other_state)
                                {
                                    i = 0;
                                    break;
                                }

                            }
                        }
                        else { i = 1; }
                    }
                    else if (other_state) { i = 0; }
                    else
                    {
                        if (i > 0 && i < 5) { i++; }
                        else { i = 0; }
                    }
                    if (i != 0 || ctrl_state || other_state || flag)
                    {
                        Console.WriteLine("{0} {1} {2} {3}", ctrl_state, other_state, flag, i);
                    }
                    if (flag)
                    {
                        var point = GetMousePosition();
                        Console.WriteLine("{0} {1}", point.X, point.Y);
                        var data = Clipboard.GetDataObject();
                        if (data == null) continue;
                        if (!data.GetDataPresent(typeof(string))) continue;
                        var str = (string)data.GetData(typeof(string));
                        Console.WriteLine(str);
                        if (str == "") continue;
                        var strs = new List<string>();

                        {
                            var bufs = str.Split(new char[] { '\n', '\r', ' ', '.', ',', '　', '(', ')', ':', '/', '_' }, StringSplitOptions.RemoveEmptyEntries);
                            //小文字に置き換え
                            for (int j = 0; j < bufs.Length; j++)
                            {
                                bufs[j] = bufs[j].ToLower();
                            }
                            if (bufs.Length == 0) continue;

                            for (int j = 3; j >= 1; j--)
                            {
                                if (j == 3 && bufs.Length >= 3)
                                {
                                    str = bufs[0] + "+" + bufs[1] + "+" + bufs[2];
                                }
                                else if(j==2 && bufs.Length >= 2)
                                {
                                    str = bufs[0] + "+" + bufs[1];
                                }
                                else if(j==1) { str = bufs[0]; }
                                else { str = ""; }
                                if (!Regex.IsMatch(str, @"^[a-z+]+$")) continue;
                                strs.Add(str);
                            }
                        }

                        if (strs.Count == 0) continue;
                        if (window != null) continue;

                        var dpiXProperty = typeof(SystemParameters).GetProperty("DpiX", BindingFlags.NonPublic | BindingFlags.Static);
                        var dpiYProperty = typeof(SystemParameters).GetProperty("Dpi", BindingFlags.NonPublic | BindingFlags.Static);
                        var dpiRateX = 96.0 / (int)dpiXProperty.GetValue(null, null);
                        var dpiRateY = 96.0 / (int)dpiYProperty.GetValue(null, null);

                        window = new Window
                        {
                            Title = "My User Control Dialog",
                            Content = new Control(strs.ToArray()),
                            Width = 400,
                            Height = 400,
                            Topmost = true,
                            Top = point.Y * dpiRateY,
                            Left = point.X * dpiRateX,

                        };
                        window.PreviewKeyDown += new KeyEventHandler(HandleEsc);
                        window.Closed += new EventHandler(NullWindow);
                        window.ShowDialog();
                    }
                }
            }
            finally
            {
                if (hasHandle) { mutex.ReleaseMutex(); }
                mutex.Close();
            }

        }
        static private void HandleEsc(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) { window.Close(); }
        }
        static private void NullWindow(object sender, EventArgs e) { window = null; }
    }
}
