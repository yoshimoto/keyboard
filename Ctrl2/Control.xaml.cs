﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Ctrl2
{
    public partial class Control : UserControl
    {
        static private Tuple<string, bool> GetWeblioData(string str)
        {
            string result = "";
            WebClient client = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8
            };
            string data = "";
            try
            {
                data = client.DownloadString("http://ejje.weblio.jp/content/" + str);
            }
            catch (WebException)
            {
                return Tuple.Create("WebException", false);
            }

            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(data);

            //見つかった場合
            var found = htmlDoc.DocumentNode.SelectNodes(@"//td[@class=""content-explanation ej""]");
            if (found != null)
            {
                var pos_means = found.Select(a => a.InnerText.Trim()).ToArray<string>();
                foreach (var item in pos_means)
                {
                    result += string.Format("{0}\n", item);
                }
                if (result != "") { return Tuple.Create(result,true); }
            }

            //見つからなかった場合
            var miss = htmlDoc.DocumentNode.SelectNodes(@"//span[@class=""nrCntSgT""]");
            if (miss != null)
            {
                result += "Not found.\n\n";
                var pos_words = htmlDoc.DocumentNode
                    .SelectNodes(@"//span[@class=""nrCntSgT""]")
                    .Select(a => a.InnerText.Trim()).ToArray<string>();

                var pos_mean = htmlDoc.DocumentNode
                    .SelectNodes(@"//div[@class=""nrCntSgB""]")
                    .Select(a => a.InnerText.Trim()).ToArray<string>();

                if (pos_words.Length == pos_mean.Length)
                {
                    for (int i = 0; i < pos_words.Length; i++)
                    {
                        result += string.Format("{0} {1}\n", pos_words[i], pos_mean[i]);
                    }
                }
            }
            if (result == "") { result = "No data"; }
            return Tuple.Create(result, false);
        }
        static private Tuple<string, bool> GetCollinsData(string str)
        {
            string result = "";
            WebClient client = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8
            };
            string data = "";
            try
            {
                data = client.DownloadString("https://www.collinsdictionary.com/dictionary/english/" + str);
            }
            catch (WebException)
            {
                return Tuple.Create("WebException", false);
            }

            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(data);


            {
                var defs = htmlDoc.DocumentNode.SelectNodes(@"//div[@class=""def""]");
                if (defs != null)
                {
                    int NDef = 0;
                    for (int i = 0; i < defs.Count; i++)
                    {
                        var def = defs[i];
                        result += (NDef + 1).ToString() + ": " + Regex.Replace(def.InnerText, "<[^>]*?>", "") + "\n";
                        NDef++;
                        if (NDef >= 2) break;
                    }
                    if (result != "") { return Tuple.Create(result, true); }
                }
            }

            //見つからなかった場合
            if (result == "") { result = "No data"; }
            return Tuple.Create(result, false);
        }
        public Control(string[] strs)
        {
            InitializeComponent();
            Result.Text = "";

            for (int i = 0; i < strs.Length; i++)
            {
                var str = strs[i];
                string result = "";
                bool flag = false;
                (result, flag) = GetWeblioData(str);
                if (flag || i == strs.Length - 1)
                {
                    Word.Text = str;
                    Result.Text += "Weblio:\n" + result + "\n";
                    break;
                }
            }
            for (int i = 0; i < strs.Length; i++)
            {
                var str = strs[i];
                string result = "";
                bool flag = false;
                (result, flag) = GetCollinsData(str);
                if (flag || i == strs.Length - 1)
                {
                    Result.Text += "Collins:\n" + result + "\n";
                    break;
                }
            }
        }

        private void Word_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Process.Start("http://ejje.weblio.jp/content/" + Word.Text);
        }
    }
}
